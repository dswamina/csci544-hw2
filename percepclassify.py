import math
import sys
from collections import defaultdict
file_input=str(sys.argv[1])

file_testing=str(sys.argv[2])
#open the model file
model = open(file_input,'r',errors='ignore')
#open the output file 

#open the input file
input1 = open(file_testing,'r',errors='ignore')
#create a dictionary to put the probability for each class
prob_dict={}
class_prob_dict={}
class_name_dict={}
words_weight={}
#read each line in the input file and store in a list 'input_words'
input_words=[]
class_count=0
for line in model:
 if line.startswith("**__CLASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS__**"):
  words=line.split()
  class_name_dict[class_count]=words[1]
  #class_prob_dict[class_count]=words[2]
  words_weight[class_count]=defaultdict(int)
  class_count+=1
  line=next(model)
 l=line.strip().split()
 #print (l)
 #print(l.length)
 words_weight[class_count-1][l[0]]=l[1]
result ={}
correct=0
wrong=0
total=0
for line in input1:
 words = line.split()
 actual = words[0]
 for i in class_name_dict:
  result[i]=0
  for w in words:
   result[i]+=int(words_weight[i][w])
 index=max(result,key=result.get)
 print(class_name_dict[index])
# if(actual==class_name_dict[index]):
#  correct+=1
#  total+=1
# else: 
#  wrong+=1
#  total+=1
#print (wrong/total)
