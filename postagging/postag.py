import sys
import glob
import errno
from collections import defaultdict
import itertools
import codecs
model = str(sys.argv[1])
f2=open('POSTEST','w',errors='ignore')
m = open(model,'r',errors='ignore')
prob_dict={}
class_prob_dict={}
class_name_dict={}
words_weight={}
#read each line in the input file and store in a list 'input_words'
input_words=[]
class_count=0

####### Determining Class Labels and Words Weight!

for line in m:
 if line.startswith("**__CLASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS__**"):
  words=line.split()
  #print(words)
  class_name_dict[class_count]=words[1]
  #class_prob_dict[class_count]=words[2]
  words_weight[class_count]=defaultdict(int)
  class_count+=1
  line=next(m)
 l=line.strip().split()
 #print (l)
 #print(l.length)
 words_weight[class_count-1][l[0]]=l[1]


#### Reading Input from Command line till 'quit' is typed!#################
sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
#a=list(iter(input,'quit'))
for line in sys.stdin:
 words=line.split()
 l=len(words)
 for i,word in enumerate(words):
  if i!=0:
   prevword=words[i-1]
  elif i==0:
   prevword='bb'
  if i != l-1:
   nextword=words[i+1]
  elif i==l-1:
   nextword='ee'
  
  f2.write('CURR:'+str(word)+' '+'PREV:'+str(prevword)+' '+'NEXT:'+str(nextword))
  f2.write('\n')
f2.close()
################# Tagging the Output! ###################################################3
with open('POSTEST','r') as f3:
 for line in f3:
  words=line.split()
  currword=words[0].split(':')
  weights=defaultdict(int)
  for i in words_weight:
   for w in words:
    weights[i]+=int(words_weight[i][w]) # automatically assigns zero to words not in the dictionary
  index = max(weights,key=weights.get)
  sys.stdout.write(currword[1]+'/'+class_name_dict[index]+' ') 
  if words[2]=='NEXT:ee': 
   sys.stdout.write('\n') 
