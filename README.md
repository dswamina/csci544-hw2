What is the accuracy of your POS Tagger?

Ans) 91.2651

b) What are the precision, recall and F-Score for each of the named entity types recognizer, and what is the overall F-score?

Ans) 

                                 MISC               PER                 LOC                ORG

   Precision               0.6738          0.9345               0.723                0.7367

   Recall                   0.2892          0.3461               0.5389               0.5411

   F score                 0.435            0.53219             0.6612               0.6409


c)

The accuracy of Naive Bayes Classifier is 60.4148% . The accuracy is less in Naive Bayes classifier because it uses bag of words rather than considering the positioning / grouping of words that come together. Perceptrons take as input the syntax of language - it takes the three words together than just word itself. 
